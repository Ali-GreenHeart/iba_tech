## Theoretical question

1. Describe in your own words why cycles are needed in a programming.

## The task is:

Implement a Javascript program that will find all numbers divisible by 5 (divided by 5 without residue) in a given range.

#### Technical requirements:
- Use modal browser window to read the number that will be entered by the user. 
- Output in the console all numbers divisible by 5, from 0 to the number entered by the user. If there are no such numbers, display the phrase `Sorry, no numbers'' in the console
- It is necessary to use the ES6 syntax (ES2015) when creating variables.

#### An optional advanced complexity task:
- Check that the user has entered an integer number. If the user has not entered an integer, ask again to enter a valid number until the integer will be entered.
- Receive from the user two numbers, `m ` and `n `. To deduce in a console all simple numbers (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE) in a range from `m ` to `n `. The smaller of the entered numbers will be `m`, the larger will be `n`. If at least one of the numbers does not follow the validation condition above, display an error message and ask both numbers again.

#### Literature:
- [Loops while, for](https://learn.javascript.info/while-for)
