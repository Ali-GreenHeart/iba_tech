## The task is:
Create a table where they will change color by clicking on cells.

#### Technical requirements:
- Create a field 30*30 of white cells with the help of the element ``` <table>```.
- At the click on a white cell it should change colour on black. By clicking on a black cell it should change colour back on white.
- The table itself should not be inserted into the source HTML code, and generated and added to the DOM page using Javascript.
- The event handler `click` should be placed on the whole table. Events pop up from the element up the DOM tree, they can all be caught with one event handler on the table, and it determines which cell the user clicked.
- At the click on any place of the document out of the table, all colours of cells should be changed on opposite (the hint: it is necessary to put Event Listener on `<body> `).
- To change the colors of all cells at once, do not need to traverse them with the cycle. If you mark the clicked cells with a specific class, you can repaint them all at once in one step - changing the class on the table.

#### Literature:
- [Bubbling and capturing](https://javascript.info/bubbling-and-capturing)
