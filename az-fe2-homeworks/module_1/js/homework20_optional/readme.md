### This task is not necessary to perform

## The task is:
Implement a function that will allow you to evaluate if the development team will be able to deliver the project before the deadline.

## Technical requirements:
- The function takes three parameters as input:
  - an array of numbers that represents the speed of different team members. The number of elements in the array means the number of people in the team. Each element means how many story-points (conditional assessment of task complexity) can be performed by this developer in 1 day.
  - An array of numbers, which is a backlog (a list of all tasks to be performed). The number of elements in the array means the number of tasks in the backlog. Each number in the array means the number of story-points needed to perform this task.
  - deadline date (Date object).
- After execution, the function should calculate if the development team will be able to complete all the tasks from the backlog before the deadline (starting from today). If it is true, display the following message: `All tasks will be successfully completed ?  days before the deadline!`. Insert the necessary number of days into the text. If not, display the message ` The team of developers should spend in addition ? hours after a deadline to execute all problems in a backlog `
- Work goes on for 8 hours a day on workdays.  

#### Literature:
- [Date and time](https://javascript.info/date)