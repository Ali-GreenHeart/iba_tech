### This task is not necessary to perform

## The task is:
Draw a circle on the page using the parameters entered by the user.

## Technical requirements:
- Show the button with the text "Draw a circle" on it when loading a page. This button should be the only content in the body of the HTML document, the rest of the content should be created and added to the page using Javascript.
- When you click on the button "Draw a circle" show one input field - the diameter of the circle. When you click on the "Draw" button, create 100 circles (10*10) with a random color on the page. When clicking on a particular circle - this circle should disappear, and the empty space should be filled, which means that all other circles are moved to the left.
- You may want to put the event handler on each lap to make it disappear. This is ineffective, so you don't have to do it. There should only be one event handler per page.

#### Literature:
- [Searching in DOM](https://javascript.info/searching-elements-dom)
- [Modifying the document](https://javascript.info/modifying-document)
- [Introduction to browser events](https://javascript.info/introduction-browser-events)
- [Bubbling and capturing](https://javascript.info/bubbling-and-capturing)