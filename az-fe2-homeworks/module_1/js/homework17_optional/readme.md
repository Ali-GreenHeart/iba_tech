### This task is not necessary to perform

## The task is:
Implement the function to calculate the n-th generalized Fibonacci number.

## Technical requirements:
- Write a function to calculate the n-th generalized [Fibonacci number] (https://ru.wikipedia.org/wiki/%D0%A7%D0%B8%D1%81%D0%BB%D0%B0_%D0%A4%D0%B8%D0%B1%D0%BE%D0%BD%D0%B0%D1%87%D1%87%D0%B8). The entry arguments are three numbers - `F0`, `F1`, `n`, where `F0`, `F1` are the first two numbers of the sequence (can be any integers), `n` is the order number of the Fibonacci number to be found. The sequence will be built according to the following rule `F2 = F0 + F1`, `F3 = F1 + F2` and so on.
- Get number which will be entered by the user (`n `), with the help of modal window of a browser.
- Calculate the n-th number in the generalized Fibonacci sequence and display it on the screen.
- The user can enter a negative number - result is necessary to count under the same rule (`F-1 = F-3 + F-2 `).

#### Literature:
- [Recursion, stack](https://javascript.info/recursion)