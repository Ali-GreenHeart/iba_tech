### This task is not necessary to perform

## Theoretical question:

1. Write down how do you understand recursion. What is the practical usege of recursion?

## The task is:

Implement the function of calculating the factorial number. 

#### Technical requirements:
- Get the number that the user will enter using the modal browser window.
- Calculate the factorial of the number and display it on the screen.
- Use ES6 syntax to work with variables and functions.\

#### An optional advanced complexity assignment:
- After entering the data, add a check for correctness. If the user has entered alphabetic characters, or has entered not a number, ask for a new number (the default value for it should be the information entered earlier).

#### Literature:
- [Recursion, stack](https://javascript.info/recursion)