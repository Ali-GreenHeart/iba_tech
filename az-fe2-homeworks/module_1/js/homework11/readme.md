## Theoretical question:

1. Why is the keyboard events not recommended for working with input?

## The task is:

Implement the function of backlighting of pressed keys

#### Technical requirements:
- The `index.html` file contains the markup for the buttons. 
- Each button contains the name of the key on the keyboard
- As soon as you press the specified keys, the button where this letter is written should be colored blue. If some other letter has been colored blue before, it becomes black. For example, by pressing `Enter` the first button is colored blue. Further, the user presses `S `, and the button `S ` is painted in blue, and the button `Enter ` again becomes black.

#### Литература:
-  [Keyboard: keyup, keydown, keypress](https://javascript.info/keyboard-events) 