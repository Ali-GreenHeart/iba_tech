## The task is:

Implement the possibility of changing the color theme of the site by the user.

#### Technical requirements:
- Take any finished HTML/CSS homework assignment.
- Add the "Change theme" button to the page.
- By clicking on the button - change the color scheme of the site (colors of buttons, background, etc.) as you wish. When clicking again - return everything as it was initially - as if two color themes are available for the page.
- The selected theme should be saved after the page reload

#### Literatire:
- [About the LocalStorage](https://alligator.io/js/introduction-localstorage-sessionstorage/)