## Theoretical question:

1. Explain in your own words how you understand what an event handler is

## The task is:

Create a field for entering the price with validation.

#### Technical requirements:
- When a page is loaded, show the user an input field (`input`) with `Price` inscription. This field will be used to enter numeric values
- The behavior of the field should be as follows: 
   - When you focus on the input field, it should have a green frame. If the focus is lost, green frame disappears.
   - You need to read the value of the field only when the focus is removed from the field. Then create a `span` above the field, with the text : `Current price: ${value from the input field}`. There should be a `button` with a cross (`X`) next to `span`. The value inside the input field is colored green.
   - When you click on `X` - `span` with text and the `X` button should be removed. The value entered in the input field is reset to zero.
   - If the user has entered number less than 0 - highlight an input field with a red frame, and display a phrase under a field - `Please enter correct price`, when the focus is lost. In this case you don't need to create a `span` with the value of the field.
- There are some examples of realisation of an input field with created `span` in the `img` folder.

#### Literature:
- [Searching DOM elements](https://javascript.info/searching-elements-dom)
- [Adding and deleting DOM elements](https://javascript.info/modifying-document)
- [Introduction into browser events](https://javascript.info/introduction-browser-events)