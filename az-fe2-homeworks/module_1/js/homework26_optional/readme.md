### This task is not necessary to perform\

## The task is:
Реализовать слайдер на чистом Javascript.

#### Technical requirements:
- Create HTML markup containing `Previous`, `Next` buttons and images (6 pieces), which will be scrolled horizontally. 
- Only one picture should be visible on the page. The `Previous` button will be on the left and the `Next` button will be on the right.
- By pressing the button `Next` - the new picture following from the list should appear.
- After clicking on the `Previous` button, the previous picture should appear.
- The slider should be infinite, i.e. if you press the `Previous` button at the beginning, the last picture will appear, and if you press the `Next` button, when the visible one is the last picture, the first picture will be visible.
- An example of a slider can be seen [here](http://kenwheeler.github.io/slick/) (first example).