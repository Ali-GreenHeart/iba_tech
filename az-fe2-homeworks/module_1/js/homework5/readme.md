## Theoretical question.

1. Describe in your own words what shielding is and what it is for in programming languages

## The task is:

Add methods that will return the user's age and password to the result of the previous homework.

#### Technical requirements:
- Take the completed homework number 4 (the function `createNewUser()` you created) and add the following functionality to it:
   1. When you call the function it should ask the caller for the date of birth (the text in the format `dd.mm.yyyyy`) and save it in the field `birthday`.
   2. Create a method `getAge()` which will return how many years the user is.
   3. Create a method `getPassword()` that will return the first letter of the username in upper case, connected to the last name (lower case) and the year of birth. (e.g. `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`).
- The result of the `createNewUser()` function and `getAge()` and `getPassword()` functions of the created object should be output to the console.

#### Literature:
- [Date and time](https://learn.javascript.info/date)