function createUser(name,surname,date) {
    const newUser={
        firstname: name,
        lastname: surname,
        birthday: date,
        getLogin(name,surname) {
            return(name.slice(0,1)+surname.toLowerCase());
        },
        getAge(){
            date1=parseInt(createUser().birthday.slice(-4));
            let d=new Date();
            return(d.getFullYear()-date1);
        }
    };
    return newUser;
}
console.log(createUser("ahmad","isiyev","02-03-2000"),createUser().getLogin("ahmad","Isiyev"),createUser().getAge());