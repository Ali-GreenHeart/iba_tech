### This task is not necessary to perform

## The task is:
Implement the function of complete cloning of the object.

## Technical requirements:
- Write a function for recursive full cloning of the object (without a single pass by reference, the internal nesting of the object properties can be quite large).
- The function must successfully copy properties in the form of objects and arrays at any nesting level.
- It is impossible to use built-in cloning mechanisms in the code, such as the `Object.assign()` function or the spread operator. 

#### Literature:
- [Objects](https://javascript.info/object)
- [Destructuring](https://javascript.info/destructuring-assignment)
- [Recursion, stack](https://javascript.info/recursion)
- [Array.isArray()](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray)