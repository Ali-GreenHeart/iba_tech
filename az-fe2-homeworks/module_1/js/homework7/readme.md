## Theoretical question:

1. Explain in your own words how you understand what is Document Object Model (DOM)

## The task is:

Implement a function that will receive an array of items and display them on the page as a list.

#### Technical requirements:
- Create a function that will take an array as an argument.
- Print each of the array elements on the page as a list item
- It is necessary to use template lines and function `map ` for creating the content of the list before displaying it on the page.
- Examples of arrays that can be displayed: 
   ```javascript
   ['hello', 'world', 'Baku', 'IBA Tech Academy', '2019']
   ```
   
   ```javascript
   ['1', '2', '3', 'sea', 'user', 23]
   ```
- You can use any array you like

#### Optional advanced complexity task:
- Clear the page in 10 seconds. Show the countdown timer (seconds only) before clearing the page.
- If there is another array or object inside the array, display it as a sublist.

#### Literature:
- [Searching DOM elements](https://javascript.info/searching-elements-dom)
- [Modifying elements](https://javascript.info/modifying-document)
- [Template string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)
- [Array.prototype.map()](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/map)