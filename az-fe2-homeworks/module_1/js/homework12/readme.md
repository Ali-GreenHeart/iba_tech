## Theoretical questions:

1. Describe in your own words the difference between the `setTimeout()` and `setInterval()` functions.
2. What happens if a zero delay will be passed to the `setTimeout()` function? Will it work instantly, and why?
3. Why is it important to remember to call the `clearInterval()` function when you don't need a previously created launch cycle anymore?

## The task is:

Implement a program that will show different images cyclically.

#### Technical requirements:
- The folder `banners` contains HTML code and a folder with pictures.
- At program start the first picture should be displayed on the screen.
- In 10 seconds instead of it the second picture should be shown.
- In another 10 seconds - the third.
- In another 10 seconds - the fourth.
- After all pictures will appear - this cycle should start all over again.
- At program start somewhere on the screen there should be a button with an inscription ' Stop `.
- On pressing the button ` to Stop ` a cycle comes to the end. The picture which has been shown at the moment of pressing the button remains and does not change any more.
- Next to the button `Stop` should be the button `Resume`, by pressing which the cycle continues with the picture, which is currently shown on the screen.
- Markup can be changed, add the necessary classes, id, attributes, tags.

#### Optional advanced complexity task:
- When you start the program, there should be a timer with seconds and milliseconds on the screen, showing how much time is left until the next picture is shown.
- Hide the picture and show the new picture gradually (fadeOut / fadeIn animation) for 0.5 seconds.

#### Literature:
- [setTimeout and setInterval](https://javascript.info/settimeout-setinterval)