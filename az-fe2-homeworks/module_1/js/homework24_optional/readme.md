### This task is not necessary to perform\

## The task is:
Write the implementation of the game ["Mine sweeper"](http://minesweeper.odd.su/).

#### Technical requirements:
- Draw an 8*8 field on the screen (you can use a table or a set of blocks).
- Randomly generate 10 mines in the field. The user does not see where they are.
- Left-clicking on a field cell "opens" its contents to the user. 
  - If there is a mine in this cell, the player has lost. In this case, show all other mines on the field. Other actions become unavailable, you can only start a new game. 
  - If there is no mine, show the number - how many mines are next to this cell.
  - If the cell is empty (there are no mines next to it) - it is necessary to open all neighboring cells with numbers.
- Right-clicking the mouse sets or clears the checkbox of the mine from the "closed" cell.
- After the first move there should appear the button "Start the game again", which will reset the previous result and re-initialize the field.
- The number of flags placed above the field should be displayed, and the total number of minutes (e.g. `7 / 10`).

##### An optional advanced complexity task:
- When double-clicking on a cell with a number - if the same number of check boxes is set around it as on the number of this cell, open all neighboring cells.
- Add the possibility for the user to specify the size of the field independently. It is possible to consider quantity of mines on a field under the formula ` Quantity of mines = quantity of cells / 6 `.

#### Literature:
- [Searching in DOM](https://javascript.info/searching-elements-dom)
- [Modifying the document](https://javascript.info/modifying-document)
- [Bubbling and capturing](https://javascript.info/bubbling-and-capturing)