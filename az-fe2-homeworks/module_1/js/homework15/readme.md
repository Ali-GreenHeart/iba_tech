## The task is:

Add various effects using jQuery to your HTML/CSS #6 (Flex/Grid) homework

#### Technical requirements:
- Add a horizontal menu at the top of the page with a link to all sections of the page.
- When clicking on a link - smoothly scroll down the page to the selected place.
- If you scroll down more than 1 screen, the "Up" button with fixed positioning should appear at the bottom right. When clicking on it, smoothly scroll to the top of the page.
- Add the button under one of sections which will execute function `slideToggle () ` (to hide and show on click) for the given section.

#### Literature:
- [Anchor links](https://www.rapidtables.com/web/html/link/html-anchor-link.html#same-page)
- [Ultimate jQuery guide](https://www.tutorialrepublic.com/jquery-tutorial/)
- [Standars animation effects in jQuery](https://api.jquery.com/category/effects/)