### This task is not necessary to perform\

## The task is:
Make a Javascript calculator using the ready-made [layout] (calculator.zip).

#### Technical requirements:
- The archive contains the template of the calculator layout. It is necessary to make this calculator work.
- When clicking on the numeric keys, the set of entered numbers should be shown on the calculator's display.
- At the click on signs of operators (`* `, `/`, `+ `, `-`) on a board there is nothing occurs - the program waits for introduction of the second number for operation performance.
- If the user has entered one number, has chosen the operator, and has entered the second number at pressing both buttons `= `, and any of operators in a board should appear result of execution of the previous expression.
- At pressing of keys ` M + ` or ` M - ` in the left part of a board it is necessary to show the small letter ` m ` it means that number is stored in memory. Clicking on `MRC` will show the number from memory on the screen. Pressing `MRC` again should clear the memory.

##### An optional advanced complexity task:
- Все клавиши калькулятора должны также нажиматься с клавиатуры. Нажатие `Enter` будет развнозначно нажатию `=`

#### Literature:
- [Searching in DOM](https://javascript.info/searching-elements-dom)
- [Modifying the document](https://javascript.info/modifying-document)
- [Bubbling and capturing](https://javascript.info/bubbling-and-capturing)