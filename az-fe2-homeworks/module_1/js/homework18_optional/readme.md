### This task is not necessary to perform

## The task is:
Create a student object and analyze the student's scoreboard.

## Technical requirements:
- Create an empty object `student`, with fields `name` and `last name`. 
- Ask the user name and surname of the student, the received values should be written in corresponding fields of the object.
- Ask the user for the subject name and grade using the loop. If the user presses Cancel when n-query about the name of the subject, stop the cycle. Write all grades for all subjects in the property of the student  called `grades`.
- Calculate the number of bad (less than 4) grades by subject. If there are no such grades, display the message `The student is transferred to the next course`. 
- Calculate the average grade point by subjects. If it is greater than 7, output the message 'The student has a scholarship'.

#### Literature:
- [Objects](https://javascript.info/object)
- [For in statement](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/for...in)