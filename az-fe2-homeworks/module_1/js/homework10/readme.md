## The task is:

Write the implementation of the "Show password" button.

#### Technical requirements:
- The `index.html` file contains the markup for two password fields. 
- By clicking on the icon next to a particular field - the symbols entered by the user should be displayed, the icon is changing its appearance.
- When the password is not visible - the field icon should look like the one in the first field (Enter password)
- When an icon is clicked, it should look like the one in the second field (Enter password)
- After clicking on the Confirm button, you need to compare the entered fields values.
- If the values match - display a modal window (alert) with the text - `You are welcome`;
- If the value does not match - display the red text under the second field `You need to enter the identical values`
- After pressing the button the page should not be reloaded
- You can change the markup, add attributes, tags, id, classes and so on.
