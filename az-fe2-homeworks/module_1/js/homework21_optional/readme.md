### This task is not necessary to perform

## The task is:
Implement a universal object array filter

## Technical requirements:
- Write a function `filterCollection()`, which will allow you to filter any array by specified keywords.
- The function must take three main arguments into account:
  - an array to be filtered
  - a string with keywords to be found inside the array (one word or several words separated by a space)
  - A boolean flag that tells you whether you need to find all the keywords (`true`) or if one of them is enough (`false`)
  - the fourth and next arguments will be the names of the fields within which you need to look for a match. If the field is not on the first level of the object, it should be specified the full path through `.`. Level of nesting of fields can be any.
- Example of function call:

```javascript
filterCollection(vehicles, 'en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description')
```
   In this example, the `vehicles` array will be filtered with the keywords `en_US` and `Toyota`. `true` in the third parameter means that to successfully include a record in the final result there must be a match for both keywords. The last few parameters contain the names of fields to search for keywords inside. For example, `contentType.name` means that inside each object `vehicle` may be a field `contentType`, which is an object or an array of objects inside which there may be a field `name`. In this field (as well as in other specified ones) it is necessary to search for matches with keywords.
- In the example above, the `locales.name` record means that the `locales` field inside the `vehicle` object can be both an object and an array. If it is an array, it means that there are objects inside the array, each of which may have a `name` property. Finding a keyword in at least one of the array elements is enough for successful filtering.
- Different keywords can be in different properties of the object. For example, in the example above, the keyword `en_US` can be found in the field `locales.name`, while the keyword `Toyota` can, for example, be inside the property `description`. This object must be found.
- The search must be case-insensitive.

##### Note: 
This task can be used in real life. For example, if there is a data table on the page and there is a search line at the top, this function can be used to filter values in the table when entering keywords into the search line 

#### Literature:
- [Arrays](http://javascript.info/array)
- [Destructuring](https://javascript.info/destructuring-assignment)
- [Array.isArray()](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray)