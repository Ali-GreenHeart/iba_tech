### This task is not necessary to complete

## The task is:

Create page [psd](Parallax2.psd) 

#### Technical requirements for your page:
- The background images between the blocks should move slower than the main blocks with text when scrolling
- Triangular areas before and after some blocks should be made using pure CSS and take up the full width of the screen

For your convenience, the full versions of all background images are placed in a separate [folder](img) and are given together with the design
