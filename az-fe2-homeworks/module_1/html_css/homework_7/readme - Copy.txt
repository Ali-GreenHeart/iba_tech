## The task is:

Create a page with animation, like in the picture [gif](animation_front-end_wars.gif). 

#### Technical requirements for your page:
- The animation should be 30 seconds long, and then it should start over

#### Text (you can use your own version):

##### EPISODE 0
##### ATTACK OF THE FRONTEND

When there is no more hope. When Winter is coming. When the White Walkers prepare to break down the Wall and JAVA can no longer stop them ... <br><br>
Comes he - Javascript. Lord of untyped data and components, he does not give up technical tasks "to do nicely" and is not afraid of calculations on the user side. But he comes not alone, having 3 of his faithful friends with him: jQuery, React.js and Node.js.

#### An optional task:
- Play the sound of the splash track from [Star Wars](https://www.youtube.com/watch?v=EjMNNpIksaI) when opening the page.
