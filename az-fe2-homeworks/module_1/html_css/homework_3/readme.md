## The task:

Create a page similar to the  `Milan_Holidays.psd` layout.

#### Technical requirements for your page:
- The layout must have a fixed width
- All logos (both icons and text) must be made as clickable links
- The "Milan Holidays" text must be created using only text elements, no images there
- The content must be in the middle of the page and have exactly same width, as it appears on the layout
- All sizes and indents must be the same as in the layout
- Pictures and the text underneath them should be clickable links
- The arrow next to the View Details inscription should be made as a pseudo-element
- A special copyright symbol must be used in the footer
- The logo and the contacts at the top of the page, as well as all photo cards, must be positioned using the CSS  `float` property.

#### An optional advanced complexity task:
- Create a gradient frame around the images using CSS
- Add an animation (appearing from the bottom) for the orange block in the picture when hovering the mouse cursor on the block
