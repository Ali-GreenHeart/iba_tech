$('.single-item').slick();
$('.slick-bigger').slick({
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slick-smaller'
});
$('.slick-smaller').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slick-bigger',
});

const slickPrevBtn = document.querySelector(".slick-prev");
const slickNextBtn = document.querySelector(".slick-next");
const slickDragg = document.querySelector(".slick-smaller.container.slick-initialized.slick-slider");



function handler(event){
    console.log("...");
    document.querySelectorAll(".slick-smaller-item").forEach(item=>{
        item.style.transform = "translateY(8px)";
        item.style.border = "none";
    });
    const slickActiveImg = document.querySelector(".slick-smaller-item.slick-current.slick-active");
    slickActiveImg.style.transform = "translateY(-8px)";
    slickActiveImg.style.transition = "transform 0.5s ease-in";
    slickActiveImg.style.border = "2px solid #2acbac";
    // document.querySelector("div.slick-list.draggable").style.paddingTop = "20px";/*islemir deyesen*/
}

slickPrevBtn.addEventListener("click", handler);
slickNextBtn.addEventListener("click", handler);
slickDragg.addEventListener("mouseover", handler);
